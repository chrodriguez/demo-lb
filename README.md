# Ejemplo de Gitlab-CI + docker

Este repositorio, crea imagenes docker cuando se crea un tag que respeta la
nomenclatura definida por semver. 

## Consideraciones

Analizar los archivos

* `docker/Dockerfile`
* `.gitlab-ci.yml`
* `docker/docker-compose.yml`
* `.dockerignore`

## Uso de la imagen para desarrollo

```bash
cd docker
docker-compose -p demo-lb-dev -f docker-compose.yml -f docker-compose.dev.yml up
```
Una vez iniciado, podremos visualizar el sitio accediendo a http://localhost:9090.
En este ambiente es posible editar los fuentes del proyecto y los cambios serán visibles sin necesidad de reiniciar nada 

## Uso de la imagen en produccion

```bash
cd docker
docker-compose -p demo-lb up
```
